import matplotlib as mp
import plistlib


filename = "/home/superfly/PythonProjects/ItunesParser/test-data/mymusic.xml"

def findDuplicates(filename):
    print("filename {0}".format(filename))
    ## read from filename
    plist = plistlib.readPlist(filename)

    ## get tracks
    tracks = plist["Tracks"]
    trackList = {}

    ## get the tracks
    try:
        for _ , track in tracks.items():
        
            name = track["Name"]
            duration = track["Total Time"]//1000
            key = (name,duration)
            
            if key not in trackList:
                trackList[key] = 1
            else:
                trackList[key] += 1;
                print(key[0])
    except:
        pass
                
    return trackList



findDuplicates(filename)



import numpy as np
from PIL import Image, ImageDraw
import sys, os, random, argparse

def generateRandomTile(tileSize=(100,100),count=20,shapeSize=5):
    tile = Image.new(mode="RGB",size=tileSize)
    h,w = tileSize

    draw = ImageDraw.Draw(tile)
    
    for i in range(count):
        x = random.randint(0,h)
        y = random.randint(0,w)
        recSize = random.randint(1,shapeSize)
        color = (random.randint(0,255),
                 random.randint(0,255),
                 random.randint(0,255))
        draw.rectangle([x,y,x+recSize,y+recSize],fill=color)

    #tile.show()
    return tile

def generateUniformTile(tileSize=(100,100), count=5, shapeSize=10):

    tile = Image.new(mode="RGB", size=tileSize)
    h,w = tileSize

    draw = ImageDraw.Draw(tile)
    
    for i in range(count):
        for j in range(count):
            x = i*shapeSize*2
            y = j*shapeSize*2
            color = (random.randint(0,255),
                     random.randint(0,255),
                     random.randint(0,255))            
            draw.rectangle([x,y,x+shapeSize,y+shapeSize],fill="white")
    
    tile.show()
    return tile

def generateImageFromTile(tile,imageSize):
    image = Image.new(mode="RGB",size=imageSize)

    tileH,tileW = tile.size
    # the plus one roofs the rows and cols
    rows = int(imageSize[0]/tileH)+1
    cols = int(imageSize[1]/tileW)+1

    for r in range(rows):
        for c in range(cols):
            print(r,c)
            image.paste(tile,(r*tileH,c*tileW))

    image.show()
    return image

def makeAutoStereoGram(depthMap):
    
    #tile = generateUniformTile(tileSize=(100,100), count=5, shapeSize=10)
    tile = generateRandomTile(tileSize=(100,100), count=400, shapeSize=5) 
    tileImage = generateImageFromTile(tile, depthMap.size)
    
    cols, rows = depthMap.size
    image = Image.new(mode="RGB",size=depthMap.size)
    img = image.load()
    tImg = tileImage.load()
    dMap = depthMap.load()
    
    print("dimenstions {0}".format(depthMap.size))
    for j in range(rows):
        for i in range(cols):
            shift = int(dMap[i,j]/10)
            pixPos = i - tile.size[0] + shift
            if pixPos > 0 and pixPos < cols :
                tImg[i,j] = tImg[pixPos,j]
            
    #print("min {0}, max {1}".format(min,max))
    #img = Image.fromarray(npImage, 'RGB')
    tileImage.show()
    return tileImage
            
def main():
    
    #filename = "/home/superfly/PythonProjects/Autostereogram/dragonnew_map.jpg"
    #filename = "/home/superfly/PythonProjects/Autostereogram/goat.jpg"
    filename = "/home/superfly/PythonProjects/Autostereogram/earle3.jpg"
    
    depthMap = Image.open(filename)
    depthMap = depthMap.convert(mode="L")
    autostereogram = makeAutoStereoGram(depthMap)
    autostereogram.save("myASG2.png")
    

if __name__ == "__main__":
    main()



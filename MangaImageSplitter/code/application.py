import tkinter as tk
import cv2
from tkinter import messagebox
from tkinter import Frame
from tkinter import Canvas
from tkinter import Button
from tkinter import Label
from tkinter import Entry
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import askdirectory
from PIL import Image, ImageTk
import numpy as np
import json
import pickle
from pathlib import Path
import os
from PyPDF2 import PyPDF2

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.grid()
        
        # Bind arrow keys to classification for speed
        master.bind('<Up>', self.classifyTileTrueEvent)
        master.bind('<Down>', self.classifyTileFalseEvent)
        master.bind('<Left>', self.undoClassificationEvent)
        
        # variables
        self.cv2Img = None 
        self.pilImg = None
        self.fileDir = None
        self.dirList = None
        self.dirIndex = -1
        self.dirIndexHasChanged = True
        self.tileSize = (128,128)
        self.countW = None
        self.countH = None
        self.h = 0
        self.w = 0
        self.programState = {"width" : self.w,
                             "height" : self.h,
                             "pickledFile" : None,
                             "fileDir" : None,
                             "dirList" : None,
                             "dirIndex": -1}
        
        # Gui objects
        self.frame0 = None
        self.frame1 = None
        self.canvas0 = None
        self.entry0 = None
        
        #Buttons
        self.convertPdf2ImageBtn = None
        self.selectDirBtn = None
        self.classifyTileTrueBtn = None
        self.classifyTileFalseBtn = None
        self.undoBtn = None
        self.quitBtn = None
        
        
        self.classifiedData = []
        
        self.create_widgets()
        
    def create_widgets(self):

        # frame0 contains images
        self.frame0 = Frame(self)
        self.frame0.grid(row=0,column=0)

        self.canvas0 = Canvas(self.frame0, height=300,
                              width=300, bg="blue")
        self.canvas0.grid()
        
        # frame1 containes buttons
        self.frame1 = Frame(self)
        self.frame1.grid(row=0,column=1)
        
        self.convertPdf2ImageBtn = Button(self.frame1,
                                     text="Select a PDF",
                                     command=
                                     self.convertPdf2Image)
        self.convertPdf2ImageBtn.grid(sticky="NEW")

        self.selectDirBtn = Button(self.frame1,
                                     text="Select Directory\nof Images",
                                     command=
                                     self.loadDirForClassification)
        self.selectDirBtn.grid(sticky="NEW")
                
        self.classifyTileTrueBtn = Button(self.frame1,
                                          text="Edge Exists",
                                          command=self.classifyTileTrue)
        self.classifyTileTrueBtn.grid(sticky="NEW")

        self.classifyTileFalseBtn = Button(self.frame1,
                                           text="Edge not Exists",
                                           command=self.classifyTileFalse)
        self.classifyTileFalseBtn.grid(sticky="NEW")

        self.undoBtn = Button(self.frame1,
                              text="Undo",
                              command=self.undoClassification)
        self.undoBtn.grid(sticky="NEW")


        self.loadBtn = Button(self.frame1,
                              text="Load",
                              command=self.loadSaveState)
        self.loadBtn.grid(sticky="NEW")



        self.saveBtn = tk.Button(self.frame1,
                                 text="Save",
                                 command=self.saveProgram)
        self.saveBtn.grid(sticky="NEW")

        self.entry0 = Entry(self.frame1)
        self.entry0.grid(sticky="NEW")

        self.quitBtn = tk.Button(self.frame1,
                                 text="QUIT",
                                 fg="red",
                                 command=self.master.destroy)
        self.quitBtn.grid(sticky="NE")

# Code for convertPdf2Image() provied by Sylvain
# https://stackoverflow.com/questions/2693820/extract-images-from-pdf-without-resampling-in-python
        
    # expand pdf into their images and store them in a file
    def convertPdf2Image(self):
        fileName = askopenfilename()
        # get pdf name from Path
        if( fileName is None or len(fileName) <= 0):
            return
        
        pdfName = fileName.split("/")[-1]            
        input1 = PyPDF2.PdfFileReader(open(fileName, "rb"))
        page0 = input1.getPage(0)
        xObject = page0['/Resources']['/XObject'].getObject()

        folder = os.getcwd()
        dest = "data/images/" + "".join(pdfName.split(".")[::-1])

        if not os.path.isdir(dest):
            os.mkdir(dest)
            
        for obj in xObject:
            if xObject[obj]['/Subtype'] == '/Image':
                size = (xObject[obj]['/Width'], xObject[obj]['/Height'])        
                data = xObject[obj].getData()
                if xObject[obj]['/ColorSpace'] == '/DeviceRGB':
                    mode = "RGB"
                else:
                    mode = "P"
                if xObject[obj]['/Filter'] == '/FlateDecode':
                    img = Image.frombytes(mode, size, data)
                    img.save(obj[1:] + ".png")
                    img.show()
                elif xObject[obj]['/Filter'] == '/DCTDecode':
                    print(dest + "/" + obj[1:])
                    img = open(dest + "/" + obj[1:] + ".jpg", "wb")
                    img.write(data)
                    img.close()
                elif xObject[obj]['/Filter'] == '/JPXDecode':
                    img = open(obj[1:] + ".jp2", "wb")
                    img.write(data)
                    img.show()
                    img.close()

    def saveProgram(self):
        if self.fileDir is not None:
            self.programState["width"] = self.w
            self.programState["height"] = self.h
            pickledFile = "results/" + self.entry0.get()
            self.programState["pickledFile"] = pickledFile
            self.programState["fileDir"] = self.fileDir
            self.programState["dirList"] = self.dirList
            self.programState["dirIndex"] = self.dirIndex
            
            with open("_saveState.txt", "w+") as fp:
                json.dump(self.programState, fp)
                pickle.dump(self.classifiedData,
                            open(pickledFile,"wb"))

    def loadSaveState(self):
        with open("_saveState.txt") as jsonFile:
            # if not empty, load programState
            if os.stat("_saveState.txt").st_size != 0:
                self.programState = json.load(jsonFile)
                # if previous file exists, load it                
                if Path(self.programState["fileDir"]).is_dir():
                    # restore program state            
                    self.w = self.programState["width"]
                    self.h = self.programState["height"]
                    self.fileDir =self.programState["fileDir"]
                    self.dirList = self.programState["dirList"]
                    self.dirIndex = self.programState["dirIndex"]
                    self.dirIndexHasChanged = True
                    
                    pickledFile = self.programState["pickledFile"]
                    pickledFile = pickledFile.split("/")[-1]
                    
                    self.entry0.insert(0,pickledFile)
                    loc = "results/" + pickledFile
                    self.classifiedData = pickle.load(open(loc, "rb")) 
                    self.drawSquare()
                    
    def loadDirForClassification(self):
        #get dir path
        self.fileDir = askdirectory()
        
        # check for valid dir
        self.dirList = os.listdir(self.fileDir)
        self.dirIndex = 0
        self.dirIndexHasChanged = True

        # Draw checks if dirIndexHas Changed
        # Not elegant
        self.drawSquare()
                        
    # get current tile location
    def getTileLoc(self):
        
        H,W = self.cv2Img.shape
        
        x0 = self.w * self.tileSize[0]
        x1 = x0 + self.tileSize[0]
        y0 = self.h * self.tileSize[1]
        y1 = y0 + self.tileSize[1]
        
        if x1 > W:
            x1 = W
            x0 = W - self.tileSize[0]
        if y1 > H:
            y1 = H
            y0 = H - self.tileSize[1]
            
        return (x0,y0,x1,y1)
    
    
    def drawSquare(self):

        if self.fileDir is None:
            print("select a file")
        else:

            # check to see if program has moved to next Image
            if self.dirIndexHasChanged:
                fp = self.fileDir + "/" + self.dirList[self.dirIndex]
                self.cv2Img = cv2.imread(fp, 0)
                self.dirIndexHasChanged = False
                
            img = self.cv2Img.copy()

            self.countH = self.cv2Img.shape[0] // self.tileSize[0] + 1
            self.countW = self.cv2Img.shape[1] // self.tileSize[1] + 1

            #adjust the shape of canvas to match image
            
            x0,y0,x1,y1 = self.getTileLoc()
                    
            cv2.rectangle(img,(x0,y0),(x1,y1),0)

            self.pilImg = Image.fromarray(img)
            self.pilImg = ImageTk.PhotoImage(self.pilImg)
            
            # Anchored to top left, values must be negative
            # because they are relative to anchor
            self.canvas0.config(width=self.cv2Img.shape[1])
            self.canvas0.create_image(
                (0,-y0+50), anchor="nw", image=self.pilImg)
            

    def updateLoc(self):

        print(self.w,self.h,self.countW,self.countH)
        self.w += 1
        
        if self.w >= self.countW:
            self.w = 0
            self.h += 1
            
        if self.h >= self.countH:
            # do something to signify end
            # Open another image
            self.h = 0
            self.dirIndex += 1
            self.dirIndexHasChanged = True

    def undoClassificationEvent(self, event):
        self.undoClassification()

    def undoClassification(self):

        print(self.w, self.h, self.countW, self.countH)
        
        # TODO: redesign function so it is not ugly
        # problem, need to load image before updating coords when
        # going backwards from different size images

        # do nothing if beginning
        if self.w == 0 and self.h == 0 and self.dirIndex == 0:
            return
        else:            
            size = len(self.classifiedData)
            if size > 0:
                self.classifiedData.pop(size - 1)
                
            # Wrap to previous Image
            if self.w == 0 and self.h == 0 and self.dirIndex > 0:
  
                self.dirIndex -= 1
                self.dirIndexHasChanged = True
                # Ugly
                if self.dirIndexHasChanged:
                    fp = self.fileDir + "/" + self.dirList[self.dirIndex]
                    self.cv2Img = cv2.imread(fp, 0)
                    self.dirIndexHasChanged = False

                img = self.cv2Img.copy()
                self.countH = self.cv2Img.shape[0] // self.tileSize[0] + 1
                self.countW = self.cv2Img.shape[1] // self.tileSize[1] + 1
                
                self.w = self.countW - 1
                self.h = self.countH - 1

                x0,y0,x1,y1 = self.getTileLoc()
                    
                cv2.rectangle(img,(x0,y0),(x1,y1),0)

                self.pilImg = Image.fromarray(img)
                self.pilImg = ImageTk.PhotoImage(self.pilImg)
            
                # Anchored to top left, values must be negative
                # because they are relative to anchor
                self.canvas0.config(width=self.cv2Img.shape[1])
                self.canvas0.create_image(
                    (0,-y0+50), anchor="nw", image=self.pilImg)
                
                # self.drawSquare()
            # Wrap to previous h            
            elif self.w == 0:
                self.h -= 1
                self.w = self.countW - 1
                self.drawSquare()
            # go back one
            else:
                self.w -= 1
                self.drawSquare()
        

            
    def classifyTileTrueEvent(self, event):
        self.classifyTileTrue()
        
    def classifyTileTrue(self):
        if (self.w < self.countW and
            self.h < self.countH):
            
            x0,y0,x1,y1 = self.getTileLoc()
            img = self.cv2Img[y0:y1,x0:x1].copy()
            self.classifiedData.append((img,1))
            self.updateLoc()
            self.drawSquare()
            
    def classifyTileFalseEvent(self, event):
        self.classifyTileFalse()

    def classifyTileFalse(self):
        if (self.w < self.countW and
            self.h < self.countH):
            
            x0,y0,x1,y1 = self.getTileLoc()
            img = self.cv2Img[y0:y1,x0:x1].copy()
            self.classifiedData.append((img,0))
            self.updateLoc()
            
            self.drawSquare()
            

if __name__ == "__main__":
    
    root = tk.Tk()
    root.title("Manga Classification Program")
    app = Application(master=root)
    app.mainloop()

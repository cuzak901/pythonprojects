import tkinter as tk
from tkinter import messagebox
from tkinter import Frame
from tkinter import Canvas
from tkinter import Button
from tkinter import Label
from tkinter import Entry
from tkinter import Scrollbar
from tkinter import StringVar
from tkinter import Grid
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import askdirectory
from PIL import Image, ImageTk
import numpy as np
import cv2
import json
import pickle
import glob
import re
from pathlib import Path
import os

from PyPDF2 import PyPDF2

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.grid(sticky="NEWS")
        Grid.grid_rowconfigure(self.master,0,weight=1)
        Grid.grid_columnconfigure(self.master,0,weight=1)        
       
        # Bind arrow keys to classification for speed
        master.bind('<Up>', self.scrollUp)
        master.bind('<Down>', self.scrollDown)
        master.bind('q', self.classifyTileTrueEvent)
        master.bind('w', self.classifyTileFalseEvent)
        master.bind('<Control-z>', self.undoEvent)
        master.bind('<Escape>', self.master.focus)
        master.bind('<g>', self.showPlt)

        
        # Program variables
        # Holds all Images [[cv2Img]]        
        self.cv2Imgs = None
        self.cv2ImgCanvases = None
        
        self.pilImg = None
        self.fileDir = None
        self.dirDict = None
        self.nList = None
        self.dirList = None
        self.tileSize = (128,128) #(r,c)
        
        self.page = 0
        self.lastPage = 0
        self.dataCount = 0
        
        # [[cv2Img]]
        self.dataImgStacks = None
        # [[int]]
        self.dataLabelStacks = None
        # [[cv2img]]
        self.undoStacks = None
        # [[(r,c)]]
        self.undoStacksCoords = None
        
        # image max height and width
        self.H = 0
        self.W = 0
        self.h = 0
        self.w = 0
        self.programState = {"width" : self.W,
                             "height" : self.H,
                             "pickledFile" : None,
                             "fileDir" : None,
                             "dirList" : None,
                             "dirIndex": -1}
        
        # Gui objects
        self.frame0 = None
        self.frame1 = None
        self.frame2 = None
        self.frame3 = None
        self.frame4 = None
        self.frame5 = None
        self.canvas0 = None
        self.entry0 = None
        self.entry1 = None
        self.entry2 = None
        self.label0 = None
        self.label1 = None
        self.label2 = None
        self.label3 = None
        self.label4 = None
        
        #Buttons
        self.convertPdf2ImageBtn = None
        self.selectDirBtn = None
        self.classifyTileTrueBtn = None
        self.classifyTileFalseBtn = None
        self.undoBtn = None
        self.quitBtn = None
        self.backPageBtn = None
        self.nextPageBtn = None
        self.changeTileSizeBtn = None
        
        self.classifiedData = []
        self.create_widgets()
        
    def create_widgets(self):

        # frame0 contains an image
        self.frame0 = Frame(self.master)
        self.frame0.grid(sticky="NEWS",row=0,column=0)
        Grid.grid_rowconfigure(self.frame0, 0, weight=1)
        Grid.grid_columnconfigure(self.frame0, 0, weight=1)
        
        self.canvas0 = Canvas(self.frame0, bg="blue")
        self.canvas0.grid(sticky="NEWS")
        
        # frame2 containes buttons
        self.frame2 = Frame(self.master)
        self.frame2.grid(sticky="NEWS",row=0,column=1)
        
        self.convertPdf2ImageBtn = Button(self.frame2,
                                          text="Select a PDF",
                                          command=
                                          self.convertPdf2Image)
        self.convertPdf2ImageBtn.grid(sticky="NEW")
        
        self.selectDirBtn = Button(self.frame2,
                                   text="Select Directory\nof Images",
                                   command=self.loadDirForClassification)
        self.selectDirBtn.grid(sticky="NEW")
        
        #Tile Size label and entry
        self.frame1 = Frame(self.frame2)
        self.frame1.grid(sticky="EW")

        th = "Tile Height:"
        tw = "Tile Width:"
        labelWidth = max(len("Tile Height:"),len("Tile Width:"))
        # height
        self.label1 = Label(self.frame1, text=th, width=labelWidth)
        self.label1.grid(sticky="W", row=0, column=0)

        self.entry1 = Entry(self.frame1,width=4)
        self.entry1.grid(sticky="EW",row=1,column=0)
        self.entry1.insert(0,str(self.tileSize[0]))
        # width
        self.label2 = Label(self.frame1,text=tw,width=labelWidth)
        self.label2.grid(sticky="W", row=0, column=1)

        self.entry2 = Entry(self.frame1,width=4)
        self.entry2.grid(sticky="EW",row=1,column=1)
        self.entry2.insert(0,str(self.tileSize[1]))

        self.changeTileSizeBtn = Button(self.frame1,
                                        text="Change Tile Size",
                                        command=self.changeTileSize)
        self.changeTileSizeBtn.grid(sticky="EW",columnspan=2)

        # load and save data
        self.frame5 = Frame(self.frame2)
        self.frame5.grid(sticky="EW")
        self.loadBtn = Button(self.frame5,
                              text="Load",
                              command=self.loadSaveState)
        self.loadBtn.grid(sticky="EW",row=0,column=0)

        self.saveBtn = tk.Button(self.frame5,
                                 text="Save",
                                 command=self.saveProgram)
        self.saveBtn.grid(sticky="WE",row=0,column=1)
        self.undoBtn = tk.Button(self.frame5,
                                 text="Undo",
                                 command=self.undo)
        self.undoBtn.grid(sticky="W",row=0,column=2)

        # export
        self.exportDataBtn = Button(self.frame2,
                                    text = "Export Data",
                                    command = self.exportData)
        self.exportDataBtn.grid(sticky="EW")
        
        # File Name label and entry
        self.label0 = Label(self.frame2,text="File Save Name:")
        self.label0.grid(sticky="W")
        
        self.entry0 = Entry(self.frame2)
        self.entry0.grid(sticky="NEW")

        self.frame4 = Frame(self.frame2)
        self.frame4.grid(sticky="EW")
        
        self.backPageBtn = tk.Button(self.frame4,
                                 text="Back",
                                     command=self.backPage)
        self.backPageBtn.grid(sticky="WE",row=0,column=0)
        
        self.nextPageBtn = tk.Button(self.frame4,
                                 text="Next",
                                     command=self.nextPage)
        self.nextPageBtn.grid(sticky="WE",row=0,column=1)        

        self.quitBtn = tk.Button(self.frame4,
                                 text="QUIT",
                                 fg="red",
                                 command=self.master.destroy)
        self.quitBtn.grid(sticky="WE",row=0,column=2)

        # Page information
        s = StringVar()
        s.set("Image Count: " + str(self.dataCount))
        self.label3 = Label(self.frame2, textvariable=s)
        self.label3.s = s
        self.label3.grid(sticky="SE", row=11)

        s = StringVar()
        s.set("Page " + str(self.page) + " of " + str(self.lastPage))
        self.label4 = Label(self.frame2, textvariable=s)
        self.label4.s = s
        self.label4.grid(sticky="SW",row=11)        
        
    def updateGui(self):
        self.label3.s.set("Data Count: " + str(self.dataCount))
        self.label4.s.set("Page " + str(self.page) +
                          " of " + str(self.lastPage))
        
# Code for convertPdf2Image() provied by Sylvain
# https://stackoverflow.com/questions/2693820/extract-images-from-pdf-without-resampling-in-python
        
    # expand pdf into their images and store them in a file
    def convertPdf2Image(self):
        fileName = askopenfilename()
        # get pdf name from Path
        if( fileName is None or len(fileName) <= 0):
            return
        
        pdfName = fileName.split("/")[-1]            
        input1 = PyPDF2.PdfFileReader(open(fileName, "rb"))
        page0 = input1.getPage(0)
        xObject = page0['/Resources']['/XObject'].getObject()

        folder = os.getcwd()
        dest = "data/images/" + "".join(pdfName.split(".")[::-1])

        if not os.path.isdir(dest):
            os.mkdir(dest)
            
        for obj in xObject:
            if xObject[obj]['/Subtype'] == '/Image':
                size = (xObject[obj]['/Width'], xObject[obj]['/Height'])        
                data = xObject[obj].getData()
                if xObject[obj]['/ColorSpace'] == '/DeviceRGB':
                    mode = "RGB"
                else:
                    mode = "P"
                if xObject[obj]['/Filter'] == '/FlateDecode':
                    img = Image.frombytes(mode, size, data)
                    img.save(obj[1:] + ".png")
                    img.show()
                elif xObject[obj]['/Filter'] == '/DCTDecode':
                    print(dest + "/" + obj[1:])
                    img = open(dest + "/" + obj[1:] + ".jpg", "wb")
                    img.write(data)
                    img.close()
                elif xObject[obj]['/Filter'] == '/JPXDecode':
                    img = open(obj[1:] + ".jp2", "wb")
                    img.write(data)
                    img.show()
                    img.close()

    def saveProgram(self):        
        # Json Data
        # self.fileDir = None
        # self.dirDict = None
        # self.nList = None
        # self.tileSize = (128,128) #(r,c)
        
        # self.page = 0
        # self.lastPage = 0
        # self.dataCount = 0
        
        # # image max height and width
        # self.H = 0
        # self.W = 0
        # self.h = 0
        # self.w = 0
        
        # Pickled data
        # self.dataImgStacks = None
        # self.dataLabelStacks = None
        # self.undoStacks = None
        # self.undoStacksCoords = None
        # self.cv2Imgs = None
        # self.cv2ImgCanvases = None        
        
        if self.fileDir is not None:
            pickledFile = "results/" + self.entry0.get()
            self.programState["pickledFile"] = pickledFile
            self.programState["fileDir"] = self.fileDir
            self.programState["dirDict"] = self.dirDict
            self.programState["nList"] = self.nList
            self.programState["tileSize"] = self.tileSize
            
            self.programState["page"] = self.page
            self.programState["lastPage"] = self.lastPage
            self.programState["dataCount"] = self.dataCount
            self.programState["W"] = self.W
            self.programState["H"] = self.H
            self.programState["w"] = self.w                        
            self.programState["h"] = self.h               

            saveData = (self.dataImgStacks,
                        self.dataLabelStacks,
                        self.undoStacks,
                        self.undoStacksCoords,
                        self.cv2Imgs,
                        self.cv2ImgCanvases)
            
            with open("_saveState.txt", "w+") as fp:
                json.dump(self.programState, fp)
                pickle.dump(saveData,
                            open(pickledFile,"wb"))

    def loadSaveState(self):
        # Json Data
        # self.fileDir = None
        # self.dirDict = None
        # self.nList = None
        # self.tileSize = (128,128) #(r,c)
        
        # self.page = 0
        # self.lastPage = 0
        # self.dataCount = 0
        
        # # image max height and width
        # self.H = 0
        # self.W = 0
        # self.h = 0
        # self.w = 0
        
        # Pickled data
        # self.dataImgStacks = None
        # self.dataLabelStacks = None
        # self.undoStacks = None
        # self.undoStacksCoords = None
        # self.cv2Imgs = None
        # self.cv2ImgCanvases = None
        
        with open("_saveState.txt") as jsonFile:
            # if not empty, load programState
            if os.stat("_saveState.txt").st_size != 0:
                self.programState = json.load(jsonFile)
                # if previous file exists, load it                
                if Path(self.programState["fileDir"]).is_dir():
                    # restore program state            
                    self.fileDir = self.programState["fileDir"]
                    self.dirDict = self.programState["dirDict"]
                    self.nList = self.programState["nList"]
                    self.tileSize = self.programState["tileSize"]
                    
                    self.page = self.programState["page"]
                    self.lastPage = self.programState["lastPage"]
                    self.dataCount = self.programState["dataCount"]
                    
                    # image max height and width
                    self.H = self.programState["H"]
                    self.W = self.programState["W"]
                    self.h = self.programState["h"]
                    self.w = self.programState["w"]
                    
                    pickledFile = self.programState["pickledFile"]
                    pickledFile = pickledFile.split("/")[-1]
                    
                    self.entry0.insert(0,pickledFile)
                    loc = "results/" + pickledFile

                    saveData = pickle.load(open(loc, "rb"))                    
                    self.dataImgStacks = saveData[0]
                    self.dataLabelStacks = saveData[1]
                    self.undoStacks = saveData[2]
                    self.undoStacksCoords = saveData[3]
                    self.cv2Imgs = saveData[4]
                    self.cv2ImgCanvases = saveData[5]
                    
                    self.refreshScreen((self.w,-self.h))

    def exportData(self):
        # combine the stacks of data into one numpy array
        # [[img x][ img y][][][][][]]        
        imgBlock = np.array([np.array(stack).reshape(
            (len(stack), self.tileSize[0], self.tileSize[1]))
                             for stack in self.dataImgStacks])
        imgBlock = np.concatenate(imgBlock)
        print(len(imgBlock), imgBlock.shape)
        # combine the stacks of labels into one list of labels
        # [[x][y][][][][][]]        
        labelBlock = np.array([np.array(stack,dtype=int)
                               for stack in self.dataLabelStacks])
        labelBlock = np.concatenate(labelBlock)
        print(len(labelBlock),labelBlock)
        
        # save data in a pickle
        finalData = (labelBlock,imgBlock)
        fileLoc = "results/extractedData.pickle"
        print(fileLoc)
        pickle.dump(finalData, open(fileLoc,"wb"))
        
    def nextPage(self):
        if self.page < self.lastPage:
            self.page += 1;
            self.refreshScreen((0,0))
                            
    def backPage(self):
        if self.page > 0:
            self.page -= 1;            
            self.refreshScreen((0,0))
            
    def loadDirForClassification(self):
        # get dir path and use glob to get images
        self.fileDir = askdirectory()
        fList = glob.glob(self.fileDir + "/*.jpg")
        # find num at end of jpg file, split ".",
        # get num convert to int 
        self.nList = [int(re.findall("\d+.jpg$",file)[-1].split(".")[0])
                      for file in fList]
        self.dirDict = {self.nList[i]:fList[i]
                        for i in range(len(self.nList))}
        self.nList = sorted(self.nList)
        
        self.page = 0
        self.lastPage = self.nList[-1]

        # get file to load on canvases
        self.cv2Imgs = [cv2.imread(self.dirDict[i], 0)
                        for i in self.nList]
        # this maybe a shallow copy
        self.cv2ImgCanvases = self.cv2Imgs.deepcopy()
        # initialize undo stacks
        self.undoStacks = [[] for i in self.nList]
        self.undoStacksCoords = [[] for i in self.nList]
        #initialize data and label stacks
        self.dataImgStacks = [[] for i in self.nList]
        self.dataLabelStacks = [[] for i in self.nList]
        
        self.refreshScreen((0,0))
        
    def showPlt(self,event):
        cv2.imshow("Page", self.cv2Imgs[self.page])
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        
    def changeTileSize(self):
        try:
            self.master.focus()
            height = int(self.entry1.get())
            width = int(self.entry2.get())        
            self.tileSize = (height,width)
        except Exception as e:
            print(e)
            messagebox.showinfo("Number Error", "Please use numbers")
            
    """Scrolls the image down"""
    def scrollDown(self,event):
        self.h += 5
        imgBottom = self.H - self.canvas0.winfo_height()
        if self.h > imgBottom:
            self.h = imgBottom
        self.refreshScreen((0,-self.h))

    """Scrolles the Image up"""
    def scrollUp(self,event):
        self.h -= 5
        if self.h < 0:
            self.h = 0
        self.refreshScreen((0,-self.h))

    """Returns the top left corner and bottom right corner
       in a tuple with format (x0,y0,x1,y1)"""            
    def getTileLoc(self, coords):
        x,y = coords
        #self.tileSize format is (r,c)
        xmid = self.tileSize[1] // 2
        ymid = self.tileSize[0] // 2 
        x0 = x - xmid
        x1 = x + xmid
        y0 = y - ymid
        y1 = y + ymid
        # Clipping the image
        if x0 < 0:
            x0 = 0
            x1 = self.tileSize[1]
        if x1 > self.W:
            x1 = self.W
            x0 = self.W - self.tileSize[1]
        if y0 < 0:
            y0 = 0
            y1 = self.tileSize[0]
        if y1 > self.H:
            y1 = self.H
            y0 = self.H - self.tileSize[0]
            
        return (x0,y0,x1,y1)
        
    def drawSquare(self,coords):
        
        if self.fileDir is None:
            print("No directory selected")
        else:
            x0,y0,x1,y1 = self.getTileLoc(coords)
            # draw a square at coords on canvas
            # 1 subtracted from pt2 so that the outline can
            # be erased by undo
            cv2.rectangle(self.cv2ImgCanvases[self.page],
                          (x0,y0),
                          (x1-1,y1-1),
                          0)

    """Save previous images tiles in case we need 
    to undo a classification"""
    def addToUndoStack(self, coords):
        (x0,y0,x1,y1) = self.getTileLoc(coords)
        img = self.cv2ImgCanvases[self.page]
        img = img[y0:y1,x0:x1].copy()
        size = img.shape
        self.undoStacks[self.page].append(img)
        self.undoStacksCoords[self.page].append(coords)
        
    def undoEvent(self, event):
        self.undo()

    # Delete data and restore image
    def undo(self):
        try:
            # remove from data
            self.dataImgStacks[self.page].pop()
            self.dataLabelStacks[self.page].pop()
            self.dataCount -= 1

            # restore image
            img = self.undoStacks[self.page].pop()
            coords = self.undoStacksCoords[self.page].pop()
            (x0,y0,x1,y1) = self.getTileLoc(coords)        
            self.cv2ImgCanvases[self.page][y0:y1,x0:x1] = img
            self.refreshScreen((0,-self.h))
            
        except IndexError as e:
            # tried to pop and empty stack,
            pass
        except Exception as e:
            print(e)
            
    # add to data stacks and undostacks             
    def classifyTileTrueEvent(self, event):
        x = event.x
        y = event.y + self.h
        coords = (x,y)
        self.addToUndoStack(coords)
        self.drawSquare(coords)
        # 1 is interesting
        self.classifyTile(coords,label=1)
        self.refreshScreen((0,-self.h))
        
    # add to data stacks and undostacks         
    def classifyTileFalseEvent(self, event):
        x = event.x
        y = event.y + self.h
        coords = (x,y)
        self.addToUndoStack(coords)
        self.drawSquare(coords)
        # 0 is not interesting
        self.classifyTile(coords,label=0)
        self.refreshScreen((0,-self.h))

    def classifyTile(self,coords,label):
        x0,y0,x1,y1 = self.getTileLoc(coords)
        img = self.cv2Imgs[self.page]
        img = img[y0:y1,x0:x1].copy()
        self.dataCount += 1
        self.dataImgStacks[self.page].append(img)
        self.dataLabelStacks[self.page].append(label)

        # this function needs work, not elevent screenPos is (x,-y) 
    """Updates the canvas based on self.cv2ImgCanvas"""    
    def refreshScreen(self,screenPos):
        # save image size
        self.H,self.W = self.cv2Imgs[self.page].shape

        self.pilImg = Image.fromarray(self.cv2ImgCanvases[self.page])
        self.pilImg = ImageTk.PhotoImage(self.pilImg)

        #todo: fix screenPos[1] -value
        self.w = screenPos[0]
        self.h = -screenPos[1]
        
        # Anchored to top left, values must be negative
        # because they are relative to anchor (0, -var)
        self.canvas0.config(width=self.W)
        self.canvas0.create_image(screenPos,
                                  anchor="nw",
                                  image=self.pilImg)
        self.updateGui()

        

if __name__ == "__main__":
    root = tk.Tk()
    root.title("Manga Classification Program")
    app = Application(master=root)
    app.mainloop()


    

import pickle
import numpy as np
import os
import cv2
import json


fp = os.getcwd() + "/results/textOrNot.pickle"
print(fp)
saveData = pickle.load(open(fp,"rb"))

fp2 = "_saveState.txt"
programState = json.load(open(fp2))

nList = programState["nList"]
dirDict = programState["dirDict"]
H = programState["H"]
W = programState["W"]
tileSize = programState["tileSize"]

dataImgStacks = [[] for i in nList]

cv2Imgs =  [cv2.imread(dirDict[str(i)], 0)
            for i in sorted(nList)]

dataLabelStacks = saveData[1]
undoStacksCoords = saveData[3]

# cv2.imshow("img", cv2Imgs[0])
# cv2.waitKey(0)
# cv2.destroyAllWindows()

def getTileLoc(coords):
    x,y = coords
    #self.tileSize format is (r,c)
    xmid = tileSize[1] // 2
    ymid = tileSize[0] // 2 
    x0 = x - xmid
    x1 = x + xmid
    y0 = y - ymid
    y1 = y + ymid
    # Clipping the image
    if x0 < 0:
        x0 = 0
        x1 = tileSize[1]
    if x1 > W:
        x1 = W
        x0 = W - tileSize[1]
    if y0 < 0:
        y0 = 0
        y1 = tileSize[0]
    if y1 > H:
        y1 = H
        y0 = H - tileSize[0]
        
    return (x0,y0,x1,y1)

for page, stack in enumerate(undoStacksCoords):
    H,W = cv2Imgs[page].shape
    for coord in stack:
        print("coord {}, page {}".format(coord,page))
        x0,y0,x1,y1 = getTileLoc(coord)
        img = cv2Imgs[page]
        img = img[y0:y1,x0:x1].copy()
        dataImgStacks[page].append(img)

print(len(dataImgStacks))
print(sum([len(s) for s in dataImgStacks]))

def exportData(dataLabelStacks,dataImgStacks):
    # combine the stacks of data into one numpy array
    # [[img x][ img y][][][][][]]
    for i, s in enumerate(dataImgStacks):
        print("idx {} {}".format(i,len(s)))

    imgBlock = np.array([np.array(stack)
                         for stack in dataImgStacks])
    imgBlock = np.concatenate(imgBlock)
    print(len(imgBlock), imgBlock.shape)
    # combine the stacks of labels into one list of labels
    # [[x][y][][][][][]]        
    labelBlock = np.array([np.array(stack,dtype=int)
                               for stack in dataLabelStacks])
    labelBlock = np.concatenate(labelBlock)
    print(len(labelBlock),labelBlock)
    
    # save data in a pickle
    finalData = (labelBlock,imgBlock)
    return finalData

finalData = exportData(dataLabelStacks,dataImgStacks)
    
fileLoc = "results/extractedData2.pickle"
print(fileLoc)
pickle.dump(finalData, open(fileLoc,"wb"))


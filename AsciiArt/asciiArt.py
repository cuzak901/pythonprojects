import numpy as np
from PIL import Image



def convert2grayscale(imageFile):

    try:
        grayscaleImage = Image.open(imageFile)
        grayscaleImage = grayscaleImage.convert(mode="L")
    except:
        print("Image not found")
        exit()
    ##grayscaleImage.show()
    return grayscaleImage


def getAvgImgMap(image, scale, cols):
    H,W = image.size
    # get the size of each tile
    print("height {0}, width {1}".format(H,W)) 

    tileWidth = int(W/cols)
    tileHeight = tileWidth/scale
    rows = int(H/tileHeight)
    
    

    print(tileHeight, tileWidth, rows)
    imageArray = np.array(image)

    avgImgMap = np.zeros((rows,cols))
    
    for r in range(rows):
        for c in range(cols):
            r0 = int(r*tileHeight)
            r1 = int((r+1)*tileHeight)
            c0 = int(c*tileWidth)
            c1 = int((c+1)*tileWidth)
            avgImgMap[r,c] = int(np.average(imageArray[r0:r1,c0:c1]))

    print("avg image map \n", avgImgMap)
    return avgImgMap
    
def convert2AsciiArt(imageMap, scale):

    
    scaleSize = len(scale)
    asciiArt = []

    print("scalesize",scaleSize)
    print("convert2asciiArt fun")
    for row in imageMap:
        for col in row:
#            print( scale[int(col/255 * (scaleSize-1))] )
            asciiArt.append(scale[int(col/255 * (scaleSize-1))])
        asciiArt.append("\n")

    asciiArt = "".join(asciiArt)
    print(asciiArt)
    

    return asciiArt
    



def main():

    imagefile = "/home/superfly/PythonProjects/AsciiArt/circle_game-300x300.jpg"
    #imagefile = "/home/superfly/PythonProjects/AsciiArt/smiley.jpeg"
    grayscaleImage = convert2grayscale(imagefile)
    imageMap = getAvgImgMap(grayscaleImage, scale=.43, cols=40)

    grayscale1 = ":. "
    asciiArt = convert2AsciiArt(imageMap,grayscale1)
    
    

    

if __name__ == "__main__":
    main()

    

